// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gfs

import (
	"context"
	"fmt"
	"strings"
	"time"

	"cloud.google.com/go/firestore"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

// RecordExportWithRetry record and export doc to firestore
func RecordExportWithRetry(ctx context.Context,
	firestoreClient *firestore.Client,
	exportCollectionName string,
	exportName string,
	processed bool,
	maxRetry int,
	ev glo.EntryValues) (err error) {

	documentPath := fmt.Sprintf("%s/%s", exportCollectionName, strings.ReplaceAll(exportName, "/", "\\"))
	var exportRecord ExportRecord
	exportRecord.Processed = processed
	exportRecord.Timestamp = time.Now()
	exportRecord.StepStack = ev.StepStack
	if maxRetry == 0 {
		maxRetry = 1
	}

	var i int
	for i = 0; i < maxRetry; i++ {
		_, err = firestoreClient.Doc(documentPath).Set(ctx, exportRecord)
		if err != nil {
			glo.LogInfo(ev, "firestoreClient.Doc(documentPath).Set(ctx, exportRecord)", fmt.Sprintf("iteration %d err %v", i, err))
			time.Sleep(time.Duration(i) * 100 * time.Millisecond)
		} else {
			return nil
		}
	}
	return err
}
