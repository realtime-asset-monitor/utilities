// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gfs

import (
	"bytes"
	"context"
	"fmt"
	"log"
	"os"
	"strings"
	"testing"
	"time"

	"cloud.google.com/go/firestore"
	"github.com/google/uuid"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func TestIntegRecordExportWithRetry(t *testing.T) {
	var testCases = []struct {
		name               string
		exportName         string
		stepsType          string
		processed          bool
		wantMsgContains    []string
		wantErrMsgContains string
	}{
		{
			name:       "export",
			processed:  false,
			exportName: "t2022-03-10T10-13-07-553485523Z_org167600783936_resource_b3dbcccac2eba261ee27a9a12ddf86b23acb5e2351136f1cf8818bf4d7413238",
			stepsType:  "A",
		},
		{
			name:       "child_export",
			processed:  true,
			exportName: "t2022-03-10T10-13-07-553485523Z_org167600783936_resource_b3dbcccac2eba261ee27a9a12ddf86b23acb5e2351136f1cf8818bf4d7413238/appengine.googleapis.com/Application/0.1645001436590350.2022-02-16T08_50_36Z.child1",
			stepsType:  "B",
		},
		{
			name:       "fs_error",
			exportName: ".",
			wantMsgContains: []string{
				"firestoreClient.Doc(documentPath).Set(ctx, exportRecord)",
				"InvalidArgument",
			},
			wantErrMsgContains: "rpc error: code = InvalidArgument desc = Document name",
		},
	}
	projectID := os.Getenv("PROJECT_ID")
	if projectID == "" {
		t.Skip("Missing env var PROJECT_ID")
	}
	ctx := context.Background()
	firestoreClient, err := firestore.NewClient(ctx, projectID)
	if err != nil {
		log.Fatal(err)
	}

	exportCollectionName := "TestIntegRecordExportWithRetry"
	var ev glo.EntryValues
	ev.CommonEntryValues.Environment = "test"
	ev.CommonEntryValues.InitID = fmt.Sprintf("%v", uuid.New())
	ev.CommonEntryValues.LogOnlySeveritylevels = "INFO WARNING NOTICE CRITICAL"
	ev.CommonEntryValues.MicroserviceName = "test"
	ev.Step.StepID = "0123456789"

	var step glo.Step
	var stepsA glo.Steps
	var stepsB glo.Steps
	step.StepID = "//pubsub.googleapis.com/projects/qwerty-ram-qa-100/topics/actionTrigger/4197048377431061"
	step.StepTimestamp, _ = time.Parse(time.RFC3339, "2022-03-10T11:39:40.942Z")
	stepsA = append(stepsA, step)
	step.StepID = "launch/qwerty-ram-qa-100/ram/launch-00032-tev/caiexport/624a1b9e-f983-4874-b655-97504374a816"
	step.StepTimestamp, _ = time.Parse(time.RFC3339, "2022-03-10T11:39:44.015574975Z")
	stepsA = append(stepsA, step)
	step.StepID = "qwerty-ram-qa-100-exports/t2022-03-10T11-39-47-200547934Z_fld800047992631_iam_policy_7929110e74d513b78cb7b24b8f9cdc7f2bbe5f3f344eec5027a819de0f076013/bigquery.googleapis.com/Table/0/1646912421053924"
	step.StepTimestamp, _ = time.Parse(time.RFC3339, "2022-03-10T11:40:21.064Z")
	stepsB = append(stepsA, step)
	step.StepID = "//storage.googleapis.com/projects/_/buckets/qwerty-ram-qa-100-exports/4197054077676643"
	step.StepTimestamp, _ = time.Parse(time.RFC3339, "2022-03-10T11:40:21.064044Z")
	stepsB = append(stepsB, step)

	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			switch tc.stepsType {
			case "B":
				ev.StepStack = stepsB
			default:
				ev.StepStack = stepsA
			}

			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()

			err := RecordExportWithRetry(ctx,
				firestoreClient,
				exportCollectionName,
				tc.exportName,
				tc.processed,
				10,
				ev)

			msgString := buffer.String()
			// t.Logf("%v", msgString)
			for _, wantMsg := range tc.wantMsgContains {
				wantMsg := wantMsg
				if !strings.Contains(msgString, wantMsg) {
					t.Errorf("want msg to contains: %s", tc.wantMsgContains)
				}
			}
			if err != nil {
				if tc.wantErrMsgContains == "" {
					t.Errorf("Want no error and got %v", err)
				} else {
					if !strings.Contains(err.Error(), tc.wantErrMsgContains) {
						t.Errorf("Want error msg contains %s and got %s", tc.wantErrMsgContains, err.Error())
					}
				}
			} else {
				documentPath := fmt.Sprintf("%s/%s", exportCollectionName,
					strings.ReplaceAll(tc.exportName, "/", "\\"))
				_, err = firestoreClient.Doc(documentPath).Get(ctx)
				if err != nil {
					t.Errorf("Cannot get the export record doc, got error %v", err)
				}
			}
		})
	}
	err = DeleteCollection(ctx,
		firestoreClient,
		firestoreClient.Collection(exportCollectionName),
		100)
	if err != nil {
		log.Fatalln(err)
	}
}
