// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gfs

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"cloud.google.com/go/firestore"
	"github.com/google/uuid"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func TestIntegGetDoc(t *testing.T) {
	var testCases = []struct {
		name            string
		collectionID    string
		feedPath        string
		retriesNumber   int
		docName         string
		retryOnNotFound bool
		wantFound       bool
		wantMsgContains string
	}{
		{
			name:          "existing_doc",
			collectionID:  "testcache01",
			feedPath:      "testdata/cache_feeds_01.json",
			retriesNumber: 10,
			docName:       "//bigquery.googleapis.com/projects/brunore-ram-prd-023/datasets/ram",
			wantFound:     true,
		},
		{
			name:            "not_existing_doc",
			collectionID:    "testcache01",
			feedPath:        "testdata/cache_feeds_01.json",
			retriesNumber:   10,
			docName:         "qwerty",
			wantFound:       false,
			wantMsgContains: "not_found_in_firestore",
		},
		{
			name:            "not_existing_doc_with_retry",
			collectionID:    "testcache01",
			feedPath:        "testdata/cache_feeds_01.json",
			retriesNumber:   2,
			docName:         "qwerty",
			retryOnNotFound: true,
			wantFound:       false,
			wantMsgContains: "code = NotFound",
		},
		{
			name:            "invalid_ref",
			collectionID:    "/testcache01",
			feedPath:        "",
			retriesNumber:   10,
			docName:         "qwerty",
			wantFound:       false,
			wantMsgContains: "firestore: nil DocumentRef",
		},
	}

	projectID := os.Getenv("PROJECT_ID")
	if projectID == "" {
		t.Skip("Missing env var PROJECT_ID")
	}

	ctx := context.Background()
	firestoreClient, err := firestore.NewClient(ctx, projectID)
	if err != nil {
		log.Fatalf("firestore.NewClient %v", err)
	}
	var ev glo.EntryValues
	ev.CommonEntryValues.Environment = "test"
	ev.CommonEntryValues.InitID = fmt.Sprintf("%v", uuid.New())
	ev.CommonEntryValues.LogOnlySeveritylevels = "INFO WARNING NOTICE CRITICAL"
	ev.CommonEntryValues.MicroserviceName = "test"
	ev.Step.StepID = "0123456789"

	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			if tc.feedPath != "" {
				p := filepath.Clean(tc.feedPath)
				if !strings.HasPrefix(p, "testdata/") {
					panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
				}
				b, err := os.ReadFile(p)
				if err != nil {
					log.Fatalln(err)
				}
				var feedMessages []cai.FeedMessageFS
				err = json.Unmarshal(b, &feedMessages)
				if err != nil {
					log.Fatalln(err)
				}
				for _, feedMessage := range feedMessages {
					documentPath := tc.collectionID + "/" + strings.ReplaceAll(feedMessage.Asset.Name, "/", "\\")
					_, err = firestoreClient.Doc(documentPath).Set(ctx, feedMessage)
					if err != nil {
						log.Fatalf("firestore doc set %v", err)
					}
				}
			}

			documentPath := tc.collectionID + "/" + strings.ReplaceAll(tc.docName, "/", "\\")

			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			_, found := GetDocWithRetry(ctx,
				firestoreClient,
				documentPath,
				tc.retriesNumber,
				tc.retryOnNotFound,
				ev)
			msgString := buffer.String()
			// t.Logf("%s", msgString)

			if tc.wantFound != found {
				t.Errorf("want found %v and got %v", tc.wantFound, found)
			}

			if !tc.wantFound {
				if !strings.Contains(msgString, tc.wantMsgContains) {
					t.Errorf("want msg to contains '%s' and got \n'%s'", tc.wantMsgContains, msgString)
				}
			}

			if tc.feedPath != "" {
				err = DeleteCollection(ctx,
					firestoreClient,
					firestoreClient.Collection(tc.collectionID),
					100)
				if err != nil {
					log.Fatalln(err)
				}
			}
			logEntriesString := strings.Split(msgString, "\n")
			for _, logEntryString := range logEntriesString {
				var logEntry glo.Entry
				err = json.Unmarshal([]byte(logEntryString), &logEntry)
				if !(err != nil) {
					if logEntry.MicroserviceName == "" {
						t.Errorf("logEntry.MicroserviceName is null should not %s", logEntry.Message)
					}
					if logEntry.Environment == "" {
						t.Errorf("logEntry.Environment is null should not %s", logEntry.Message)
					}
				}
			}

		})
	}
}
