// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gfs

import (
	"context"
	"fmt"
	"strings"
	"time"

	"cloud.google.com/go/firestore"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

// GetDocWithRetry check if a document exist with retries
func GetDocWithRetry(ctx context.Context,
	firestoreClient *firestore.Client,
	documentPath string,
	maxRetry int,
	retryOnNotFound bool,
	ev glo.EntryValues) (*firestore.DocumentSnapshot, bool) {
	var documentSnap *firestore.DocumentSnapshot
	var err error
	var i int
	for i = 0; i < maxRetry; i++ {
		documentSnap, err = firestoreClient.Doc(documentPath).Get(ctx)
		if err != nil {
			// Retry are for transient, not for doc not found
			if strings.Contains(strings.ToLower(err.Error()), "notfound") && !retryOnNotFound {
				glo.LogInfo(ev, "not_found_in_firestore", documentPath)
				return documentSnap, false
			}
			if strings.Contains(strings.ToLower(err.Error()), "nil documentref") || strings.Contains(strings.ToLower(err.Error()), "permissions") {
				glo.LogInfo(ev, "nil documentref", err.Error())
				return documentSnap, false
			}
			glo.LogInfo(ev, "redo_on_transient", fmt.Sprintf("iteration %d firestoreClient.Doc(documentPath).Get(ctx) %v", i, err))
			time.Sleep(time.Duration(i) * 100 * time.Millisecond)
		} else {
			// Found
			return documentSnap, documentSnap.Exists()
		}
	}
	return documentSnap, false
}
