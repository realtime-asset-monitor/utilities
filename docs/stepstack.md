# stepstack

apply to all:

- `global.step` is based on the triggering cloud event context info
  - `global.step.StepID` is build from the triggering cloud event source and id
  - `global.step.StepTimestamp` is the timestamp of the triggering cloud event
- `global.StepStack`
  - extract the stepstack from the triggering event data
  - append the step crafted from the triggering cloud event context info
- Latencies (current and e2e) are computed after performing the targeted action and before the finish logentry
- so
  - the stepstack track the chain of events triggering this execution
  - the latency measure this execution time and the e2e time

## convertfeed

- global.stepstack initiate as nil
- case batch: caiFeed has a stepstack as was created by dumpinventory / splitdump
  - then append the triggering event step to the caiFeed stepstack
- case real-time: caiFeed does not have a stepstack as was created by Cloud Asset Inventory
  - create the originating step using
    - the feed message `StartTime` as the timestamp meaning:
      - in real time : when the change occur
      - in batch : when the dump was triggered
    - the asset name + feed message timestamp as id
