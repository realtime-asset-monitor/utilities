// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package str

import (
	"crypto/sha256"
	"encoding/base64"
)

// Hash used sha1 to hash a string to a shorter one
func Hash(s string) string {
	hasher := sha256.New()
	hasher.Write([]byte(s))
	// sha256 returns a 256 bits value, 8 bytes
	// base64 encoding returns a 4*(n/3) rounded up to a multiple of 4, so 44 characters.
	// bigquery insertID limit is 128 characters
	return base64.URLEncoding.EncodeToString(hasher.Sum(nil))
}
