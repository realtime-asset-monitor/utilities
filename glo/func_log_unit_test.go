// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package glo

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"testing"
	"time"

	"github.com/google/uuid"
)

func TestUnitLog(t *testing.T) {
	var testCases = []struct {
		name                  string
		logOnlySeveritylevels string
	}{
		{
			name:                  "dolog",
			logOnlySeveritylevels: "INFO WARNING NOTICE CRITICAL",
		},
		{
			name:                  "nolog",
			logOnlySeveritylevels: "",
		},
	}
	var s Step
	s.StepID = "teststep"
	var sStack Steps
	sStack = append(sStack, s)
	ComEv := CommonEntryValues{
		Environment:      "dev",
		InitID:           fmt.Sprintf("%v", uuid.New()),
		MicroserviceName: "testing",
	}
	var ev EntryValues
	ev.CommonEntryValues = ComEv
	ev.Step = s
	ev.StepStack = sStack

	for _, tc := range testCases {
		tc := tc // https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		ev.CommonEntryValues.LogOnlySeveritylevels = tc.logOnlySeveritylevels
		t.Run(fmt.Sprintf("%s_LogStartCloudEvent", tc.name), func(t *testing.T) {
			var cloudEventJSONBytes []byte
			now := time.Now()
			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			LogStartCloudEvent(ev, cloudEventJSONBytes, 0, &now)
			msgString := buffer.String()

			if tc.name == "nolog" {
				if msgString != "" {
					t.Errorf("want no log message and got %s", msgString)
				}
			} else {
				if msgString == "" {
					t.Errorf("want a log message did not get it")
				}
			}
		})

		t.Run(fmt.Sprintf("%s_LogCriticalNoRetry", tc.name), func(t *testing.T) {
			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			LogCriticalNoRetry(ev, "blabla", "blabla", "blabla")
			msgString := buffer.String()

			if tc.name == "nolog" {
				if msgString != "" {
					t.Errorf("want no log message and got %s", msgString)
				}
			} else {
				if msgString == "" {
					t.Errorf("want a log message did not get it")
				}
			}
		})

		t.Run(fmt.Sprintf("%s_LogCriticalRetry", tc.name), func(t *testing.T) {
			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			LogCriticalRetry(ev, "blabla", "blabla", "blabla")
			msgString := buffer.String()

			if tc.name == "nolog" {
				if msgString != "" {
					t.Errorf("want no log message and got %s", msgString)
				}
			} else {
				if msgString == "" {
					t.Errorf("want a log message did not get it")
				}
			}
		})

		t.Run(fmt.Sprintf("%s_LogWarning", tc.name), func(t *testing.T) {
			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			LogWarning(ev, "blabla", "blabla")
			msgString := buffer.String()

			if tc.name == "nolog" {
				if msgString != "" {
					t.Errorf("want no log message and got %s", msgString)
				}
			} else {
				if msgString == "" {
					t.Errorf("want a log message did not get it")
				}
			}
		})

		t.Run(fmt.Sprintf("%s_LogInfo", tc.name), func(t *testing.T) {
			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			LogInfo(ev, "blabla", "blabla")
			msgString := buffer.String()

			if tc.name == "nolog" {
				if msgString != "" {
					t.Errorf("want no log message and got %s", msgString)
				}
			} else {
				if msgString == "" {
					t.Errorf("want a log message did not get it")
				}
			}
		})

		t.Run(fmt.Sprintf("%s_LogFinish", tc.name), func(t *testing.T) {
			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			end := time.Now()
			LogFinish(ev, "blabla", "blabla", end, "blabla", "blabla", "blabla", "blabla", 0)
			msgString := buffer.String()

			if tc.name == "nolog" {
				if msgString != "" {
					t.Errorf("want no log message and got %s", msgString)
				}
			} else {
				if msgString == "" {
					t.Errorf("want a log message did not get it")
				}
			}
		})

		t.Run(fmt.Sprintf("%s_LogInitColdStart", tc.name), func(t *testing.T) {
			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			LogInitColdStart(ev)
			msgString := buffer.String()

			if tc.name == "nolog" {
				if msgString != "" {
					t.Errorf("want no log message and got %s", msgString)
				}
			} else {
				if msgString == "" {
					t.Errorf("want a log message did not get it")
				}
			}
		})

		t.Run(fmt.Sprintf("%s_LogInitDone", tc.name), func(t *testing.T) {
			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			LogInitDone(ev, 0)
			msgString := buffer.String()

			if tc.name == "nolog" {
				if msgString != "" {
					t.Errorf("want no log message and got %s", msgString)
				}
			} else {
				if msgString == "" {
					t.Errorf("want a log message did not get it")
				}
			}
		})
	}
}
