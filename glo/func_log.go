// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package glo

import (
	"fmt"
	"log"
	"strings"
	"time"
)

const maxLogEntryFieldSize = 102400

// LogStartCloudEvent logs the start of the entrypoint function and the triggering cloud event
func LogStartCloudEvent(ev EntryValues,
	cloudEventJSONBytes []byte,
	triggeringEventAgeSeconds float64,
	now *time.Time) {
	if strings.Contains(ev.CommonEntryValues.LogOnlySeveritylevels, "INFO") {
		description := string(cloudEventJSONBytes)
		if len(description) > maxLogEntryFieldSize {
			description = fmt.Sprintf("cloud event to large to be logged: %d", len(description))
		}
		log.Println(Entry{
			InitID:                    ev.CommonEntryValues.InitID,
			MicroserviceName:          ev.CommonEntryValues.MicroserviceName,
			Environment:               ev.CommonEntryValues.Environment,
			TriggeringEventID:         ev.Step.StepID,
			Severity:                  "INFO",
			Message:                   "start cloudevent",
			Description:               description,
			TriggeringEventAgeSeconds: triggeringEventAgeSeconds,
			TriggeringEventTimestamp:  &ev.Step.StepTimestamp,
			Now:                       now})
	}
}

// LogCriticalNoRetry logs the critical error before giving up, aka not retry
func LogCriticalNoRetry(ev EntryValues,
	description string,
	assetType string,
	ruleName string) {
	if strings.Contains(ev.CommonEntryValues.LogOnlySeveritylevels, "CRITICAL") {
		if len(description) > maxLogEntryFieldSize {
			description = fmt.Sprintf("description to large to be logged: %d", len(description))
		}
		log.Println(Entry{
			InitID:            ev.CommonEntryValues.InitID,
			MicroserviceName:  ev.CommonEntryValues.MicroserviceName,
			Environment:       ev.CommonEntryValues.Environment,
			TriggeringEventID: ev.Step.StepID,
			Severity:          "CRITICAL",
			Message:           "noretry",
			Description:       description,
			AssetType:         assetType,
			RuleName:          ruleName,
		})
	}
}

// LogCriticalRetry logs the critical error before crashing execution, aka asking for a retry
func LogCriticalRetry(ev EntryValues,
	description string,
	assetType string,
	ruleName string) {
	if strings.Contains(ev.CommonEntryValues.LogOnlySeveritylevels, "CRITICAL") {
		if len(description) > maxLogEntryFieldSize {
			description = fmt.Sprintf("description to large to be logged: %d", len(description))
		}
		log.Println(Entry{
			InitID:            ev.CommonEntryValues.InitID,
			MicroserviceName:  ev.CommonEntryValues.MicroserviceName,
			Environment:       ev.CommonEntryValues.Environment,
			TriggeringEventID: ev.Step.StepID,
			Severity:          "CRITICAL",
			Message:           "retry",
			Description:       description,
			AssetType:         assetType,
			RuleName:          ruleName,
		})
	}
}

// LogWarning logs a non blocking issue
func LogWarning(ev EntryValues, message string, description string) {
	if strings.Contains(ev.CommonEntryValues.LogOnlySeveritylevels, "WARNING") {
		if len(description) > maxLogEntryFieldSize {
			description = fmt.Sprintf("description to large to be logged: %d", len(description))
		}
		log.Println(Entry{
			InitID:            ev.CommonEntryValues.InitID,
			MicroserviceName:  ev.CommonEntryValues.MicroserviceName,
			Environment:       ev.CommonEntryValues.Environment,
			TriggeringEventID: ev.Step.StepID,
			Severity:          "WARNING",
			Message:           message,
			Description:       description,
		})
	}
}

// LogInfo logs information
func LogInfo(ev EntryValues, message string, description string) {
	if strings.Contains(ev.CommonEntryValues.LogOnlySeveritylevels, "INFO") {
		if len(description) > maxLogEntryFieldSize {
			description = fmt.Sprintf("description to large to be logged: %d", len(description))
		}
		log.Println(Entry{
			InitID:            ev.CommonEntryValues.InitID,
			MicroserviceName:  ev.CommonEntryValues.MicroserviceName,
			Environment:       ev.CommonEntryValues.Environment,
			TriggeringEventID: ev.Step.StepID,
			Severity:          "INFO",
			Message:           message,
			Description:       description,
		})
	}
}

// LogFinish logs the end of a successful execution with data enabling measurement
func LogFinish(ev EntryValues,
	message string,
	description string,
	end time.Time,
	assetInventoryOrigin string,
	assetType string,
	contentType string,
	ruleName string,
	ratePerSecond float64) {
	if strings.Contains(ev.CommonEntryValues.LogOnlySeveritylevels, "NOTICE") {
		latency := end.Sub(ev.Step.StepTimestamp)
		var latencyE2E time.Duration
		if len(ev.StepStack) > 0 {
			latencyE2E = end.Sub(ev.StepStack[0].StepTimestamp)
		}
		if len(description) > maxLogEntryFieldSize {
			description = fmt.Sprintf("description to large to be logged: %d", len(description))
		}
		latencyT2S := latencyE2E - latency
		log.Println(Entry{
			InitID:               ev.CommonEntryValues.InitID,
			MicroserviceName:     ev.CommonEntryValues.MicroserviceName,
			Environment:          ev.CommonEntryValues.Environment,
			TriggeringEventID:    ev.Step.StepID,
			Severity:             "NOTICE",
			Message:              fmt.Sprintf("finish %s", message),
			Description:          description,
			Now:                  &end,
			LatencySeconds:       latency.Seconds(),
			LatencyE2ESeconds:    latencyE2E.Seconds(),
			LatencyT2SSeconds:    latencyT2S.Seconds(),
			RatePerSecond:        ratePerSecond,
			StepStack:            ev.StepStack,
			StepStackLength:      len(ev.StepStack),
			AssetInventoryOrigin: assetInventoryOrigin,
			AssetType:            assetType,
			ContentType:          contentType,
			RuleName:             ruleName,
		})
	}
}

// LogInitColdStart logs the start of the coldstart actions
func LogInitColdStart(ev EntryValues) {
	if strings.Contains(ev.CommonEntryValues.LogOnlySeveritylevels, "INFO") {
		log.Println(Entry{
			InitID:           ev.CommonEntryValues.InitID,
			MicroserviceName: ev.CommonEntryValues.MicroserviceName,
			Severity:         "INFO",
			Message:          "coldstart",
		})
	}
}

// LogInitFatal logs a critical error and crashes the init function so that deployment fails
func LogInitFatal(ev EntryValues, description string, err error) {
	if strings.Contains(ev.CommonEntryValues.LogOnlySeveritylevels, "CRITICAL") {
		if len(description) > maxLogEntryFieldSize {
			description = fmt.Sprintf("description to large to be logged: %d", len(description))
		}
		log.Println(Entry{
			InitID:           ev.CommonEntryValues.InitID,
			MicroserviceName: ev.CommonEntryValues.MicroserviceName,
			Severity:         "CRITICAL",
			Message:          "init_failed",
			Description:      fmt.Sprintf("%s %v", description, err),
		})
	}
	log.Fatalf("INIT_FAILURE %v", err)
}

// LogInitDone logs successful end of the cold start actions and enables to measure it
func LogInitDone(ev EntryValues, latencySeconds float64) {
	if strings.Contains(ev.CommonEntryValues.LogOnlySeveritylevels, "INFO") {
		log.Println(Entry{
			InitID:           ev.CommonEntryValues.InitID,
			MicroserviceName: ev.CommonEntryValues.MicroserviceName,
			Severity:         "INFO",
			Message:          "init done",
			LatencySeconds:   latencySeconds,
		})
	}
}
