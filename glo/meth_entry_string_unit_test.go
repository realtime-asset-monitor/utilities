// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package glo

import (
	"strings"
	"testing"
)

func TestUnitEntryString(t *testing.T) {
	testCases := []struct {
		name     string
		severity string
		wants    string
	}{
		{
			name:     "no severity",
			severity: "",
			wants:    "INFO",
		},
		{
			name:     "with severity",
			severity: "NOTICE",
			wants:    "NOTICE",
		},
	}
	for _, tc := range testCases {
		tc := tc // https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			var e Entry
			e.Severity = tc.severity
			r := e.String()
			if !strings.Contains(r, tc.wants) {
				t.Errorf("Wants %s in the output string, got %s", tc.wants, r)
			}
		})
	}
}
