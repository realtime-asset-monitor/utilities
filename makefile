test: goupd gotest sast

goupd:
	go get -u -t ./...
	go mod tidy

gotest:
	go version
	go test ./... -coverprofile=coverage.out
	go tool cover -html=coverage.out -o coverage.html
	go tool cover -func=coverage.out

sast:
	gosec ./...
