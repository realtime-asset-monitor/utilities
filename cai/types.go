// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cai

import (
	"encoding/json"
	"time"

	"cloud.google.com/go/pubsub"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

// Asset Asset Metadata
// Duplicate "iamPolicy" and "assetType en ensure compatibility between format in CAI feed, aka real time, and CAI Export aka batch
type Asset struct {
	Ancestors               []string        `json:"ancestors"`
	AncestorsDisplayName    []string        `json:"ancestorsDisplayName"`
	AncestryPath            string          `json:"ancestryPath"`
	AncestryPathDisplayName string          `json:"ancestryPathDisplayName"`
	AncestryPathLegacy      string          `json:"ancestry_path,omitempty"`
	AssetType               string          `json:"assetType"`
	AssetTypeLegacy         string          `json:"asset_type,omitempty"`
	IamPolicy               json.RawMessage `json:"iamPolicy"`
	IamPolicyLegacy         json.RawMessage `json:"iam_policy"`
	Name                    string          `json:"name"`
	Owner                   string          `json:"owner,omitempty"`
	ProjectID               string          `json:"projectID,omitempty"`
	Resource                json.RawMessage `json:"resource"`
	UpdateTime              time.Time       `json:"update_time"`
	ViolationResolver       string          `json:"violationResolver,omitempty"`
}

// AssetFS similar as Asset replacing json.RawMessage type by map[string]interface{} for FireStore
type AssetFS struct {
	Ancestors               []string               `json:"ancestors" firestore:"ancestors"`
	AncestorsDisplayName    []string               `json:"ancestorsDisplayName" firestore:"ancestorsDisplayName"`
	AncestryPath            string                 `json:"ancestryPath" firestore:"ancestryPath"`
	AncestryPathDisplayName string                 `json:"ancestryPathDisplayName" firestore:"ancestryPathDisplayName"`
	AncestryPathLegacy      string                 `json:"ancestry_path,omitempty" firestore:"ancestryPathLegacy,omitempty"`
	AssetType               string                 `json:"assetType" firestore:"assetType"`
	AssetTypeLegacy         string                 `json:"asset_type,omitempty" firestore:"assetTypeLegacy,omitempty"`
	IamPolicy               map[string]interface{} `json:"iamPolicy,omitempty" firestore:"iamPolicy,omitempty"`
	IamPolicyLegacy         map[string]interface{} `json:"iam_policy,omitempty" firestore:"iamPolicyLegacy,omitempty"`
	Name                    string                 `json:"name" firestore:"name"`
	Owner                   string                 `json:"owner,omitempty" firestore:"owner,omitempty"`
	ProjectID               string                 `json:"projectID,omitempty" firestore:"projectID,omitempty"`
	Resource                map[string]interface{} `json:"resource,omitempty" firestore:"resource,omitempty"`
	UpdateTime              time.Time              `json:"update_time" firestore:"update_time"`
	ViolationResolver       string                 `json:"violationResolver,omitempty" firestore:"violationResolver,omitempty"`
}

// AssetRule combine the asset data to be analyzed with the rule code to analyze
type AssetRule struct {
	FeedMessage FeedMessage `json:"feedMessage"`
	Rule        Rule        `json:"rule"`
	StepStack   glo.Steps   `json:"step_stack,omitempty"`
}

// ComplianceStatus by asset, by rule, true/false compliance status
type ComplianceStatus struct {
	AssetInventoryOrigin    string    `json:"assetInventoryOrigin"`
	AssetInventoryTimeStamp time.Time `json:"assetInventoryTimeStamp"`
	AssetName               string    `json:"assetName"`
	AssetType               string    `json:"assetType,omitempty"`
	Compliant               bool      `json:"compliant"`
	Deleted                 bool      `json:"deleted"`
	EvaluationTimeStamp     time.Time `json:"evaluationTimeStamp"`
	RuleDeploymentTimeStamp time.Time `json:"ruleDeploymentTimeStamp"`
	RuleName                string    `json:"ruleName"`
	StepStack               glo.Steps `json:"step_stack,omitempty"`
}

// ConstraintConfig content of the constraint yaml file
type ConstraintConfig struct {
	APIVersion string `json:"apiVersion" yaml:"apiVersion"`
	Kind       string `json:"kind"`
	Metadata   struct {
		Annotations map[string]interface{} `json:"annotations"`
		Name        string                 `json:"name"`
	} `json:"metadata"`
	Spec struct {
		Match      map[string]interface{} `json:"match"`
		Parameters map[string]interface{} `json:"parameters"`
		Severity   string                 `json:"severity"`
	} `json:"spec"`
}

// FeedMessage describe and asset configuration state
type FeedMessage struct {
	Asset       Asset     `json:"asset"`
	ContentType string    `json:"contentType,omitempty"`
	Deleted     bool      `json:"deleted"`
	Origin      string    `json:"origin"`
	StepStack   glo.Steps `json:"step_stack,omitempty"`
	Window      Window    `json:"window"`
}

// FeedMessageFS similar to FeedMessage using AssetFS instead of Asset for FireStore
type FeedMessageFS struct {
	Asset       AssetFS   `json:"asset" firestore:"asset"`
	ContentType string    `json:"contentType,omitempty" firestore:"contentType,omitempty"`
	Deleted     bool      `json:"deleted" firestore:"deleted"`
	Origin      string    `json:"origin" firestore:"origin"`
	StepStack   glo.Steps `json:"step_stack,omitempty" firestore:"step_stack,omitempty"`
	Window      Window    `json:"window" firestore:"window"`
	ExpireAt    time.Time `json:"expireAt,omitempty" firestore:"expireAt,omitempty"`
}

// functionConfig is the rule name and version described as a function deployment settings for compatibility with RAMv1
type functionConfig struct {
	DeploymentTime time.Time `json:"deploymentTime"`
	Environment    string    `json:"environment"`
	FunctionName   string    `json:"functionName"`
	ProjectID      string    `json:"projectID"`
}

// Member is slightly different from admin.Member to have both group email and member email
type Member struct {
	GroupEmail  string `json:"groupEmail"`
	ID          string `json:"id"`
	Kind        string `json:"kind"`
	MemberEmail string `json:"memberEmail"`
	Role        string `json:"role"`
	Type        string `json:"type"`
}

// nonCompliance form the "deny" rego policy in a <templateName>.rego module
type nonCompliance struct {
	EvaluationTimeStamp time.Time              `json:"evaluationTimeStamp"`
	Message             string                 `json:"message"`
	Metadata            map[string]interface{} `json:"metadata"`
}

// Rule associates rego modules code and a list of constraints
type Rule struct {
	Constraints    []ConstraintConfig `json:"constraints"`
	DeploymentTime time.Time          `json:"deploymentTime"`
	Name           string             `json:"name"`
	RegoModules    map[string]string  `json:"regoModules"`
}

// Violation from the "audit" rego policy in "audit.rego" module
type Violation struct {
	ConstraintConfig ConstraintConfig  `json:"constraintConfig"`
	FeedMessage      FeedMessage       `json:"feedMessage"`
	FunctionConfig   functionConfig    `json:"functionConfig"`
	NonCompliance    nonCompliance     `json:"nonCompliance"`
	RegoModules      map[string]string `json:"regoModules"`
	StepStack        glo.Steps         `json:"step_stack,omitempty"`
}

// Window Cloud Asset Inventory feed message time window
type Window struct {
	StartTime time.Time `json:"startTime" firestore:"startTime"`
}

// WrappedPubSub structure to carry PubSub messages in Cloud Event
type WrappedPubSub struct {
	Message      pubsub.Message `json:"message"`
	Subscription string         `json:"subscription"`
}
