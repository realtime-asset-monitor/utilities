# utilities

![pipeline status](https://gitlab.com/realtime-asset-monitor/utilities/badges/main/pipeline.svg) ![coverage](https://gitlab.com/realtime-asset-monitor/utilities/badges/main/coverage.svg?min_good=80)  
go functions shared across multiple RAM microservices to limit code duplication.
