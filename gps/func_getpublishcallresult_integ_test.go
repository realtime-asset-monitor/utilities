// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gps

import (
	"bytes"
	"context"
	"fmt"
	"log"
	"os"
	"strings"
	"sync"
	"testing"

	"cloud.google.com/go/pubsub"
	"github.com/google/uuid"
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func TestIntegGetPublishCallResult(t *testing.T) {
	type Env struct {
		ProjectID string `envconfig:"project_id"`
		TopicID   string `envconfig:"cev_publish_topic_id"`
	}

	var env Env
	var ev glo.EntryValues
	var pubSubErrNumber uint64
	var pubSubMsgNumber uint64
	var waitGroup sync.WaitGroup

	err := envconfig.Process("", &env)
	if err != nil {
		t.Skipf("failed to get env vars, %v", err)
	}
	t.Logf("project_id %s cev_publish_topic_id %s", env.ProjectID, env.TopicID)

	ctx := context.Background()
	pubSubClient, err := pubsub.NewClient(ctx, env.ProjectID)
	if err != nil {
		t.Error(err)
	}
	topic := pubSubClient.Topic(env.TopicID)

	ev.CommonEntryValues.Environment = "test"
	ev.CommonEntryValues.InitID = fmt.Sprintf("%v", uuid.New())
	ev.CommonEntryValues.LogOnlySeveritylevels = "INFO WARNING NOTICE CRITICAL"
	ev.CommonEntryValues.MicroserviceName = "test_microservice"
	ev.Step.StepID = "0123456789"
	pubSubErrNumber = 0
	pubSubMsgNumber = 0

	t.Run("test_publishing", func(t *testing.T) {
		for j := 0; j <= 1; j++ {
			pubSubMessage := &pubsub.Message{
				Data: []byte(fmt.Sprintf("message %d", j)),
			}
			publishResult := topic.Publish(ctx, pubSubMessage)
			waitGroup.Add(1)
			go GetPublishCallResult(ctx,
				publishResult,
				&waitGroup,
				fmt.Sprintf("message %d", j),
				&pubSubErrNumber,
				&pubSubMsgNumber,
				ev)

		}
		waitGroup.Wait()
		if pubSubMsgNumber == 0 {
			t.Errorf("Wants some pubsub message published and got none")
		}
	})

	t.Run("test_with_error", func(t *testing.T) {
		var buffer bytes.Buffer
		log.SetOutput(&buffer)
		defer func() {
			log.SetOutput(os.Stderr)
		}()

		var EmptyPubSubMessage pubsub.Message
		publishResultB := topic.Publish(ctx, &EmptyPubSubMessage)
		waitGroup.Add(1)
		go GetPublishCallResult(ctx,
			publishResultB,
			&waitGroup,
			"empty msg",
			&pubSubErrNumber,
			&pubSubMsgNumber,
			ev)
		waitGroup.Wait()

		msgString := buffer.String()
		wantMsgContains := "One or more messages in the publish request is empty"
		if !strings.Contains(msgString, wantMsgContains) {
			t.Errorf("want msg to contains '%s' and got \n'%s'", wantMsgContains, msgString)
		}

	})
}
