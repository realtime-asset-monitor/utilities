// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gps

import (
	"context"
	"fmt"
	"sync"
	"sync/atomic"

	"cloud.google.com/go/pubsub"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

// GetPublishCallResult func to be used in go routine to scale pubsub event publish
func GetPublishCallResult(ctx context.Context,
	publishResult *pubsub.PublishResult,
	waitgroup *sync.WaitGroup,
	msgInfo string,
	pubSubErrNumber *uint64,
	pubSubMsgNumber *uint64,
	ev glo.EntryValues) {
	defer waitgroup.Done()
	_, err := publishResult.Get(ctx)
	if err != nil {
		glo.LogInfo(ev, "publishResult.Get(ctx)", fmt.Sprintf("count %d on %s: %v", atomic.AddUint64(pubSubErrNumber, 1), msgInfo, err))
		return
	}
	atomic.AddUint64(pubSubMsgNumber, 1)
}
