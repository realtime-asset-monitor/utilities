// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package cev

import (
	"context"
	"log"
	"strings"
	"testing"

	cepubsub "github.com/cloudevents/sdk-go/protocol/pubsub/v2"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/kelseyhightower/envconfig"
)

func TestIntegPublish(t *testing.T) {
	type Env struct {
		ProjectID string `envconfig:"project_id"`
		TopicID   string `envconfig:"cev_publish_topic_id"`
	}
	var env Env
	err := envconfig.Process("", &env)
	if err != nil {
		t.Skipf("failed to get env vars, %v", err)
	}
	t.Logf("project_id %s cev_publish_topic_id %s", env.ProjectID, env.TopicID)

	ctx := context.Background()
	pubsubTransport, err := cepubsub.New(ctx,
		cepubsub.WithProjectID(env.ProjectID),
		cepubsub.WithTopicID(env.TopicID))
	if err != nil {
		log.Fatalf("failed to create transport, %v", err)
	}

	c, err := cloudevents.NewClient(pubsubTransport, cloudevents.WithTimeNow(), cloudevents.WithUUIDs())
	if err != nil {
		log.Fatalf("failed to create client, %v", err)
	}

	testCases := []struct {
		name             string
		microserviceName string
		eventType        string
		projectID        string
		host             string
		data             interface{}
		wantError        bool
	}{
		{
			name:             "publishing success",
			microserviceName: "service01",
			eventType:        "evt01",
			projectID:        env.ProjectID,
			host:             "host01",
			data:             map[string]string{"hello": "world"},
			wantError:        false,
		},
		{
			name:             "missing microservice name",
			microserviceName: "",
			eventType:        "evt01",
			projectID:        env.ProjectID,
			host:             "host01",
			data:             map[string]string{"hello": "world"},
			wantError:        true,
		},
		{
			name:             "missing event type",
			microserviceName: "service01",
			eventType:        "",
			projectID:        env.ProjectID,
			host:             "host01",
			data:             map[string]string{"hello": "world"},
			wantError:        true,
		},
		{
			name:             "missing projectID",
			microserviceName: "service01",
			eventType:        "evt01",
			projectID:        "",
			host:             "host01",
			data:             map[string]string{"hello": "world"},
			wantError:        true,
		},
		{
			name:             "missing host",
			microserviceName: "service01",
			eventType:        "evt01",
			projectID:        env.ProjectID,
			host:             "",
			data:             map[string]string{"hello": "world"},
			wantError:        true,
		},
	}

	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			ce, err := Publish(ctx, tc.microserviceName, tc.eventType, tc.data, c, tc.projectID, tc.host)
			if err != nil {
				if !tc.wantError {
					t.Errorf("Want no error and got %v", err)
				}
			} else {
				if tc.wantError {
					t.Errorf("Want an error and got no error")
				}
				if strings.Contains(ce.Type(), tc.microserviceName) {
					t.Errorf("cloud event type should not contain the microservice name and it did %s", ce.Type())
				}
			}
		})
	}
}
