// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package cev

import (
	"context"
	"fmt"

	cloudevents "github.com/cloudevents/sdk-go/v2"
)

// RAMCloudEventTypePrefix prefix used for all cloud event type used by Real-time Asset Monitor
const RAMCloudEventTypePrefix = "com.gitlab.realtime-asset-monitor"

// RAMTrigram Real-time Asset Monitor trigram
const RAMTrigram = "ram"

// Publish common way for microservices to publish cloud events to pubsub
func Publish(ctx context.Context,
	microserviceName string,
	eventType string,
	data interface{},
	client cloudevents.Client,
	projectID string,
	host string) (ce cloudevents.Event, err error) {
	event := cloudevents.NewEvent()
	if len(microserviceName) == 0 {
		return event, fmt.Errorf("microserviceName cannot be empty")
	}
	if len(eventType) == 0 {
		return event, fmt.Errorf("eventType cannot be empty")
	}
	if len(projectID) == 0 {
		return event, fmt.Errorf("projectID cannot be empty")
	}
	if len(host) == 0 {
		return event, fmt.Errorf("host cannot be empty")
	}

	event.SetType(fmt.Sprintf("%s.%s", RAMCloudEventTypePrefix, eventType))
	event.SetSource(fmt.Sprintf("%s/%s/%s/%s/%s", microserviceName, projectID, RAMTrigram, host, eventType))
	err = event.SetData("application/json", data)
	if err != nil {
		return event, fmt.Errorf("%s event.SetData %v", eventType, err)
	}
	result := client.Send(ctx, event)
	if !cloudevents.IsACK(result) {
		return event, fmt.Errorf("%s error client.Send %s", eventType, result.Error())
	}
	return event, nil
}
